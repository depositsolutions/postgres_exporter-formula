{% from slspath+"/map.jinja" import postgres_exporter with context %}

postgres_exporter-remove-service:
  service.dead:
    - name: postgres_exporter
  file.absent:
    - name: /etc/systemd/system/postgres_exporter.service
    - require:
      - service: postgres_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: postgres_exporter-remove-service

postgres_exporter-remove-symlink:
   file.absent:
    - name: {{ postgres_exporter.bin_dir}}/postgres_exporter
    - require:
      - postgres_exporter-remove-service

postgres_exporter-remove-binary:
   file.absent:
    - name: {{ postgres_exporter.dist_dir}}
    - require:
      - postgres_exporter-remove-symlink

postgres_exporter-remove-env:
    file.absent:
      - name: {{ postgres_exporter.service.config_dir }}
      - require:
        - postgres_exporter-remove-binary
