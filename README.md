[![pipeline status](https://git.prod.infra.deposit/saltstack/postgres_exporter-formula/badges/master/pipeline.svg)](https://git.prod.infra.deposit/saltstack/postgres_exporter-formula/pipelines/)

# postgres_exporter-formula

A saltstack formula created to setup a [postgres_exporter](https://github.com/wrouesnel/postgres_exporter)
for Prometheus in order to allow monitoring of PostgreSQL servers using prometheus.

All configuration for the exporter is managed through environment variables instead
of flags.

# Available states

## init

The essential postgres_exporter state running both ``install``.

## install

- Download postgres_exporter release from Github into the dist dir  (defaults to /opt/postgres_exporter/dist).
- link the binary (defaults to /usr/bin/postgres_exporter).
- Register the appropriate service definition with systemd.

This state can be called independently.

## uninstall

Remove the service, binaries, and configuration files. The data itself will be kept and needs to be removed manually, just to be on the safe side.

This state must always be called independently.

# Testing

## Prerequisites
The tests are using test kitchen with [inspec](https://www.inspec.io/downloads/) as verifier, so you need to have
- Ruby installed
- Docker installed

If you want to add tests you might want to take a look at the [documentation](http://testinfra.readthedocs.io/en/latest/modules.html#) for the modules.

## Running the test
If you run the tests for the first time you might need to run ``bundle install`` first. Afterwards you can run ``kitchen test ``.  
