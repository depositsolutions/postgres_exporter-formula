describe http('http://127.0.0.1:9187/metrics') do
  its('status') { should eq 200 }
  its('body') { should match /recovery_status 0/ }
end
